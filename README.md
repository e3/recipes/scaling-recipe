# scaling conda recipe

Home: "https://gitlab.esss.lu.se/epics-modules/scaling"

Package license: GPLv3

Recipe license: BSD 3-Clause

Summary: EPICS scaling module
